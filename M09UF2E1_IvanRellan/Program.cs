﻿using System;
using System.IO;
using System.Threading;

namespace M09UF2E1_IvanRellan
{
    class Program
    {
        static void Main()
        {
            Prova();
        }

        static void CountingLines(string FileName)
        {
            string[] lines = File.ReadAllLines(FileName);

            Console.WriteLine(lines.Length);
        }

        static void Prova()
        {
            string Path = "..\\..\\..\\";

            Thread t1 = new Thread(() => CountingLines(Path + "albiononline.txt"));
            Thread t2 = new Thread(() => CountingLines(Path + "shrek.txt"));
            Thread t3 = new Thread(() => CountingLines(Path + "cuatro.txt"));
            t1.Start();
            t2.Start();
            t3.Start();
        }
    }
}
